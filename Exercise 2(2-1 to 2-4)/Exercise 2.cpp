#include<iostream>
#include<string>
#include<time.h>
#include<conio.h>

using namespace std;

int bet1(int& bet, int& playerGold)
{
	int input = 0;
	do
	{
		cout << "Please enter your bet." << endl;
		cin >> input;
	}while (input <= 0 || input > playerGold);
	bet = input;
	return input;
}

void bet2(int& bet, int& playerGold)
{
	int input = 0;
	do
	{
		cout << "Please enter your bet." << endl;
		cin >> input;
	} while (input <= 0 || input > playerGold);
	bet = input;
}

void diceRoll(int& current)
{
	int a = 0, b = 0;
	a = rand() % 6 + 1;
	b = rand() % 6 + 1;
	current = a + b;
}

void payout(int alienRoll, int playerRoll, int bet, int& playerGold)
{
	if (alienRoll == playerRoll)
	{
		cout << "draw" << endl;
	}
	else if (playerRoll == 2)
	{
		cout << "snake eyes" << endl;
		playerGold = playerGold + (bet * 3);
	}
	else if (alienRoll > playerRoll)
	{
		cout << "lose" << endl;
		playerGold = playerGold - bet;
	}
	else if (alienRoll < playerRoll)
	{
		cout << "win" << endl;
		playerGold = playerGold + bet;
	}
}

void playRound(int& playerGold, int& bet, int& alienRoll, int& playerRoll)
{
	do 
	{
		cout << "Current Player Gold: " << playerGold << endl;
		bet2(bet, playerGold);
		system("pause");
		system("cls");
		cout << "Player bet: " << bet << endl;
		diceRoll(alienRoll);
		cout << "Alien roll: " << alienRoll << endl;
		diceRoll(playerRoll);
		cout << "Player roll: " << playerRoll << endl;
		payout(alienRoll, playerRoll, bet, playerGold);
		system("pause");
		system("cls");
	} while (playerGold > 0);
	cout << "You're broke." << endl;
}

int main()
{
	srand(time(NULL));
	int bet = 0;
	int playerGold = 1000;
	int alienRoll = 0;
	int playerRoll = 0;

	playRound(playerGold, bet, alienRoll, playerRoll);

	system("pause");
	return 0;
}