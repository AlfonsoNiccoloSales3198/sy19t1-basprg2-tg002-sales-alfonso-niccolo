#include <string>

using namespace std;

class Character
{
public:
	string name;
	int curHp;
	int baseHp;
	int pow;
	int vit;
	int agi;
	int dex;
	int rps;

	void setName(string x)
	{
		name = x;
	}
	string getName()
	{
		return name;
	}

	void setBaseHp(int x)
	{
		baseHp = x;
		curHp = x;
	}
	int getBaseHp()
	{
		return baseHp;
	}

	void setCurHp(int x)
	{
		curHp = x;
	}
	int getCurHp()
	{
		return curHp;
	}

	void setPower(int x)
	{
		pow = x;
	}
	int getPower()
	{
		return pow;
	}

	void setVitality(int x)
	{
		vit = x;
	}
	int getVitality()
	{
		return vit;
	}

	void setAgility(int x)
	{
		agi = x;
	}
	int getAgility()
	{
		return agi;
	}

	void setDexterity(int x)
	{
		dex = x;
	}
	int getDexterity()
	{
		return dex;
	}

	void setRPS(int x)
	{
		rps = x;
	}
	int getRPS()
	{
		return rps;
	}

};

class MCWarrior : public Character
{
public:
	MCWarrior() 
	{

	}
	MCWarrior(string a)
	{
		name = a;
		curHp = 15;
		baseHp = 15;
		pow = 6;
		vit = 4;
		agi = 2;
		dex = 1;
		rps = 1;
	}
};

class MCAssassin : public Character
{
public:
	MCAssassin()
	{

	}
	MCAssassin(string a)
	{
		name = a;
		curHp = 10;
		baseHp = 10;
		pow = 2;
		vit = 3;
		agi = 6;
		dex = 4;
		rps = 2;
	}
};

class MCMage : public Character
{
public:
	MCMage()
	{

	}
	MCMage(string a)
	{
		name = a;
		curHp = 6;
		baseHp = 6;
		pow = 5;
		vit = 4;
		agi = 2;
		dex = 3;
		rps = 3;
	}
};

class ENWarrior : public Character
{
public:
	ENWarrior()
	{

	}
	ENWarrior(string a)
	{
		name = "Orc";
		curHp = 15;
		baseHp = 15;
		pow = 6;
		vit = 4;
		agi = 2;
		dex = 1;
		rps = 1;
	}
};

class ENAssassin : public Character
{
public:
	ENAssassin()
	{

	}
	ENAssassin(string a)
	{
		name = "Goblin";
		curHp = 10;
		baseHp = 10;
		pow = 2;
		vit = 3;
		agi = 6;
		dex = 4;
		rps = 2;
	}
};

class ENMage : public Character
{
public:
	ENMage()
	{

	}
	ENMage(string a)
	{
		name = "Necromancer";
		curHp = 6;
		baseHp = 6;
		pow = 5;
		vit = 4;
		agi = 2;
		dex = 3;
		rps = 3;
	}
};

