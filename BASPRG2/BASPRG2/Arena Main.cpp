#include<iostream>
#include<string>
#include<time.h>
#include"Character.h"

using namespace std;

void playRound(Character p1)
{
	srand(time(NULL));
	int cho = 0;
	int p1hit = 0;
	int p1hitPer = 0;
	int enhit = 0;
	int enhitPer = 0;
	int p1damage = 0;
	int endamage = 0;
	int round = 1;
	string tempo;
	Character en = Character();
	MCWarrior mcWarrior = MCWarrior();
	MCAssassin mcAssassin = MCAssassin();
	MCMage mcMage = MCMage();
	ENWarrior enWarrior = ENWarrior();
	ENAssassin enAssassin = ENAssassin();
	ENMage enMage = ENMage();
	cout << "Please input your name:" << endl;
	cin >> tempo;
	p1.setName(tempo);

	while (cho < 1 || cho > 3)
	{
		cout << "Please choose your class:" << endl;
		cout << " [1] Warrior \n [2] Assassin \n [3] Mage" << endl;
		cin >> cho;
	}
	
	switch (cho)
	{
	case 1:
		mcWarrior = MCWarrior(p1.getName()); 
		p1.setBaseHp(mcWarrior.getBaseHp());
		p1.setAgility(mcWarrior.getAgility());
		p1.setDexterity(mcWarrior.getDexterity());
		p1.setPower(mcWarrior.getPower());
		p1.setVitality(mcWarrior.getVitality());
		p1.setRPS(mcWarrior.getRPS());
		break;
	case 2:
		mcAssassin = MCAssassin(p1.getName()); 
		p1.setBaseHp(mcAssassin.getBaseHp());
		p1.setAgility(mcAssassin.getAgility());
		p1.setDexterity(mcAssassin.getDexterity());
		p1.setPower(mcAssassin.getPower());
		p1.setVitality(mcAssassin.getVitality());
		p1.setRPS(mcAssassin.getRPS()); break;
	case 3:
		mcMage = MCMage(p1.getName()); 
		p1.setBaseHp(mcMage.getBaseHp());
		p1.setAgility(mcMage.getAgility());
		p1.setDexterity(mcMage.getDexterity());
		p1.setPower(mcMage.getPower());
		p1.setVitality(mcMage.getVitality());
		p1.setRPS(mcMage.getRPS());
		break;
	}
	
	
	enWarrior = ENWarrior("name");
	enAssassin = ENAssassin("name");
	enMage = ENMage("name");

	while (p1.getCurHp() > 0) 
	{
		cout << "Round " << round << endl;
		int r = rand() % 3 + 1;
		switch (r)
		{
		case 1:
			//enWarrior = ENWarrior(en.getName());
			en.setName(enWarrior.getName());
			en.setBaseHp(enWarrior.getBaseHp());
			en.setAgility(enWarrior.getAgility());
			en.setDexterity(enWarrior.getDexterity());
			en.setPower(enWarrior.getPower());
			en.setVitality(enWarrior.getVitality());
			en.setRPS(enWarrior.getRPS()); 
			break;
		case 2:
			//enAssassin = ENAssassin(en.getName());
			en.setName(enAssassin.getName());
			en.setBaseHp(enAssassin.getBaseHp());
			en.setAgility(enAssassin.getAgility());
			en.setDexterity(enAssassin.getDexterity());
			en.setPower(enAssassin.getPower());
			en.setVitality(enAssassin.getVitality());
			en.setRPS(enAssassin.getRPS());
			break;
		case 3:
			//enMage = ENMage(en.getName()); 
			en.setName(enMage.getName());
			en.setBaseHp(enMage.getBaseHp());
			en.setAgility(enMage.getAgility());
			en.setDexterity(enMage.getDexterity());
			en.setPower(enMage.getPower());
			en.setVitality(enMage.getVitality());
			en.setRPS(enMage.getRPS());
			break;
		}

		while (p1.getCurHp() > 0 && en.getCurHp() > 0)
		{
			p1hitPer = (p1.getDexterity() / en.getAgility()) * 100;
			p1hit = rand() % 100 + 1;
			if (p1hitPer < 20)
			{
				p1hitPer = 20;
			}
			else if (p1hitPer > 80)
			{
				p1hitPer = 80;
			}
			else
			{
				p1hitPer = p1hitPer;
			}

			p1damage = p1.getPower() - en.getVitality();
			if (p1damage < 0)
			{
				p1damage = 0;
			}
			if (p1.getRPS() == 1 && en.getRPS() == 2)
			{
				p1damage = p1damage * 1.50;
			}
			else if (p1.getRPS() == 2 && en.getRPS() == 3)
			{
				p1damage = p1damage * 1.50;
			}
			else if (p1.getRPS() == 3 && en.getRPS() == 1)
			{
				p1damage = p1damage * 1.50;
			}

			enhitPer = (en.getDexterity() / p1.getAgility()) * 100;
			enhit = rand() % 100 + 1;
			if (enhitPer < 20)
			{
				enhitPer = 20;
			}
			else if (enhit > 80)
			{
				enhitPer = 80;
			}
			else
			{
				enhitPer = enhitPer;
			}

			endamage = en.getPower() - p1.getVitality();
			if (endamage < 0)
			{
				endamage = 0;
			}
			if (en.getRPS() == 1 && p1.getRPS() == 2)
			{
				endamage = endamage * 1.50;
			}
			else if (en.getRPS() == 2 && p1.getRPS() == 3)
			{
				endamage = endamage * 1.50;
			}
			else if (en.getRPS() == 3 && p1.getRPS() == 1)
			{
				endamage = endamage * 1.50;
			}

			cout << p1.getName() << " HP: " << p1.getCurHp() << "/" << p1.getBaseHp() << endl;
			cout << en.getName() << " HP: " << en.getCurHp() << "/" << en.getBaseHp() << endl;
			cout << p1.getName() << " attacks " << en.getName() << endl;
			if (p1hit >= p1hitPer)
			{
				cout << en.getName() << " takes " << p1damage << endl;
				en.setCurHp(en.getCurHp() - p1damage);
			}
			else
			{
				cout << p1.getName() << " misses " << endl;
				en.setCurHp(en.getCurHp());
			}
			cout << en.getName() << " attacks " << p1.getName() << endl;
			if (enhit >= enhitPer)
			{
				cout << p1.getName() << " takes " << endamage << endl;
				p1.setCurHp(p1.getCurHp() - endamage);
			}
			else
			{
				cout << en.getName() << " misses " << endl;
				p1.setCurHp(p1.getCurHp());
			}

			system("pause");
			system("cls");
		}

		if (en.getRPS() == 1 && en.getCurHp() <= 0)
		{
			p1.setBaseHp(p1.getBaseHp() + 3);
			p1.setCurHp(p1.getCurHp() + (p1.getBaseHp() * 0.3));
			p1.setVitality(p1.getVitality() + 3);
			round++;
		}
		else if (en.getRPS() == 2 && en.getCurHp() <= 0)
		{
			p1.setAgility(p1.getAgility() + 3);
			p1.setCurHp(p1.getCurHp() + (p1.getBaseHp() * 0.3));
			p1.setDexterity(p1.getDexterity() + 3);
			round++;
		}
		else if (en.getRPS() == 3 && en.getCurHp() <= 0)
		{
			p1.setPower(p1.getPower() + 5);
			p1.setCurHp(p1.getCurHp() + (p1.getBaseHp() * 0.3));
			round++;
		}

			enWarrior.setBaseHp(enWarrior.getBaseHp()+1);
			
			enWarrior.setAgility(enWarrior.getAgility()+1);
			enWarrior.setDexterity(enWarrior.getDexterity()+1);
			enWarrior.setPower(enWarrior.getPower()+1);
			enWarrior.setVitality(enWarrior.getVitality()+1);
			
			enAssassin.setBaseHp(enAssassin.getBaseHp() + 1);
			
			enAssassin.setAgility(enAssassin.getAgility() + 1);
			enAssassin.setDexterity(enAssassin.getDexterity() + 1);
			enAssassin.setPower(enAssassin.getPower() + 1);
			enAssassin.setVitality(enAssassin.getVitality() + 1);

			enMage.setBaseHp(enMage.getBaseHp() + 1);
			
			enMage.setAgility(enMage.getAgility() + 1);
			enMage.setDexterity(enMage.getDexterity() + 1);
			enMage.setPower(enMage.getPower() + 1);
			enMage.setVitality(enMage.getVitality() + 1);

		system("pause");
		system("cls");
	}

	if (p1.getCurHp() <= 0) 
	{
		cout << "Game over" << endl;
		cout << "Farthest progress:" << round << endl;
		cout << p1.getName() << " HP: " << p1.getCurHp() << "/" << p1.getBaseHp() << endl;
		cout << "Agi: " << p1.getAgility() << endl;
		cout << "Dex: " << p1.getDexterity() << endl;
		cout << "Pow: " << p1.getPower() << endl;
		cout << "Vit: " << p1.getVitality() << endl;
	}
}


int main()
{
	srand(time(NULL));
	Character p1 = Character();


	
	playRound(p1);

	system("pause");
	return 0;
}