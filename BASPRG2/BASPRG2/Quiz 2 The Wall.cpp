#include<iostream>
#include<string>
#include<conio.h>
#include<time.h>
#include "Node.h"

using namespace std;

int nodeCounter(int numPeople)
{
	cout << "How many soldiers are at the wall?" << endl;
	cin >> numPeople;
	return numPeople;
}

int randomNumber(int roll, int numPeople)
{
	int r = numPeople;
	srand(time(NULL));
	if (r != 0)
	{
		roll = rand() % r + 1;
	}
	return roll;
}

/*void displayWin(int roll, int i)
{
	if (i == 1)
	{
		cout << linked list player name here << " will go to seek for reinforcements." << endl;
	}
	else if (i > 1)
	{
		cout << "Result: " << endl;

		cout << insert linked list of names here depending who holds the cloak << " has drawn " << roll << endl;
		how to count the nodes using the roll as a guide and the person who rolled as 0 followed by next
		cout << linked list to get name of loser << " was eliminated" << endl;
	}
	
}*/

int checkSoldiers(Node* list)
{
	int count = 0;
	string name = list->name;
	Node* check = list;
	do
	{
		count++;
		check = check->next;
	} while (check->name != name);
	return count;
}


Node* removeSoldier(Node** list, int index)
{
	Node* cur = *list, *prev;
	int i = 0;

	while (i != index)
	{
		cur = cur->next;
		i++;
	}

	cout << cur->name << " will be eliminated." << endl;

	if (cur->name == (*list)->name)
	{
		*list = (*list)->next;
	}

	prev = cur->previous;
	//cout << prev->name << " is the soldier before " << cur->name << endl;
	prev->next = cur->next;
	prev = cur->next;
	//cout << prev->name << " is the soldier after " << cur->name << endl;
	prev->previous = cur->previous;

	return *list;
}

Node* storeData(int numPeople)
{
	Node* finalList = new Node;
	Node* cur = new Node;
	string tempName = "";
	int j;

	for (j = 0; j < numPeople; j++)
	{
		Node* temp = new Node;
		cout << "What is your name soldier? " << endl;
		cin >> tempName;

		temp->name = tempName; 
		if (j == 0)
		{
			cur = temp;
			finalList = cur;
		}
		else if (j > 0 && j < numPeople - 1)
		{
		 
			temp->previous = cur;
			cur->next = temp;
			cur = temp;
		}
		else if (j == numPeople - 1)
		{
			temp->previous = cur;
			temp->next = finalList;
			cur->next = temp;
			finalList->previous = temp;
		}
	}
	//Node* view = finalList;
	//for (j = 0; j < numPeople; j++)
	//{
		//cout << view->name << endl;
		//view = view->next;
		
	//}
	return finalList;
}

void listSoldiers(Node* finalList, int numPeople)
{
	int j;
	Node* view = finalList;
	cout << "Remaining soldiers: " << endl;
	for (j = 0; j < numPeople; j++)
	{
		cout << view->name << endl;
		view = view->next;
	}

	cout << endl;
}

void firstSoldier(Node** finalList, int index)
{
	int i = 0;
	while (i != index)
	{
		i++;
		*finalList = (*finalList)->next;
	}
}

void mainGame(int numPeople, int roll)
{
	Node* finalList;
	Node cur;
	string name;
	int i = 0;

	numPeople = nodeCounter(numPeople);

	finalList = storeData(numPeople);
	system("pause");
	system("cls");

	while (numPeople != 1)
	{
		//finalList = rearrangeList(finalList);
		i++;
		cout << "Round " << i << endl;
		roll = randomNumber(roll, numPeople);
		firstSoldier(&finalList, roll);
		listSoldiers(finalList, numPeople);
		roll = randomNumber(roll, numPeople);
		cout << finalList->name << " rolled a " << roll << endl;
		removeSoldier(&finalList, roll);
		//system("pause");
		numPeople = checkSoldiers(finalList);
		cout << "There are " << numPeople << "left." << endl;
		system("pause");
		system("cls");
	}

	cout << finalList->name << " will get reinforcements." << endl;
	
}


int main()
{
	Node* finalList;
	Node cur;
	string tempName;
	int roll = 0;
	int numPeople = 0;

	mainGame(numPeople, roll);

	system("pause");
	return 0;
}