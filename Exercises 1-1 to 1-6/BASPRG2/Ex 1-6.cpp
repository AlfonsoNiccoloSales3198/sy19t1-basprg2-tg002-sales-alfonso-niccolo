#include <string>
#include <iostream>
#include <time.h>

using namespace std;

void randomArray(int numArray[10], int size)
{
	int i;
	for (i = 0; i < size; i++)
	{
		numArray[i] = rand() % (100 - 1) + 1;
	}
	for (i = 0; i < size; i++)
	{
		cout << numArray[i] << endl;
	}
}

void arraySort(int numArray[10], int size)
{
	int temp, i, j;
	for (i = 0; i < size; i++)
	{
		for (j = i + 1; j < size; j++)
		{

			if (numArray[j] < numArray[i])
			{
				temp = numArray[i];
				numArray[i] = numArray[j];
				numArray[j] = temp;
			}
		}
	}

	for (i = 0; i < size; i++)
	{
		cout << numArray[i] << endl;
	}
}

int main()
{
	srand(time(0));
	int numArray[10] = {};
	int size = sizeof(numArray) / sizeof(numArray[0]);

	randomArray(numArray, size);
	cout << endl;
	cout << "Sorted: " << endl;
	arraySort(numArray, size);

	system("pause");
	return 0;
}