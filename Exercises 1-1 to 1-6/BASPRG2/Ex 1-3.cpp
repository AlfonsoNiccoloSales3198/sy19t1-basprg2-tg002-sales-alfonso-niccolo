#include <iostream>
#include <string>

using namespace std;

void itemCountStringArray(string items[8], string myString, bool foo)
{
	int i, n = 0;
	for (i = 0; i < 8; i++)
	{
		if (myString == items[i])
		{
			foo = true;
			n++;
		}
	}
	if (foo == false)
	{
		cout << "Item " << myString << " unavailable" << endl;
	}
	else
	{
		cout << "Item " << myString << " is available in the inventory" << endl;
		cout << "You have " << n << " " << myString << " in your inventory" << endl;
	}
}
int main()
{
	string items[8] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };
	string myString;
	bool foo = false;
	int size = sizeof(items) / sizeof(items[0]);

	cout << "What item are you looking for? " << endl;
	getline(cin, myString);
	itemCountStringArray(items, myString, foo);
	system("pause");
	return 0;
}