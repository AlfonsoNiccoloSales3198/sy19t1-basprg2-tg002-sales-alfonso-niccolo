#include <iostream>
#include <string>

using namespace std;

int factorial(int x) {
	int out = 1, y;
	if (x == 0)
	{
		cout << "0 != 1" << endl;
		return 0;
	}
	for (y = 1; y <= x; y++)
	{
		out = out * y;
	}
	cout << "The factorial of " << x << " is " << out << endl;
}

int main() {
	int num, out = 1;

	cout << "Enter the number to get the factorial of ";
	cin >> num;

	factorial(num);

	system("pause");
	return 0;
}