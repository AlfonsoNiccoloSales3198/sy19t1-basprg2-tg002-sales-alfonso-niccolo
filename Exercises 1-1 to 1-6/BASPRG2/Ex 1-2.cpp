#include <iostream>
#include <string>

using namespace std;

void printStringArray(string items[8])
{
	int i;
	for (i = 0; i < 8; i++)
	{
		cout << items[i] << endl;
	}
}
int main()
{
	string items[8] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };

	printStringArray(items);

	system("pause");
	return 0;
}