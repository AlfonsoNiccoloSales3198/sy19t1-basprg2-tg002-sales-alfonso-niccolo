#include <iostream>
#include <string>
#include <time.h>

using namespace std;

void randomArray(int numArray[10], int size)
{
	int i;
	for (i = 0; i < size; i++)
	{
		numArray[i] = rand() % (100 - 1) + 1;
	}
	for (i = 0; i < size; i++)
	{
		cout << numArray[i] << endl;
	}
}

void largestNumArray(int numArray[10], int size)
{
	int i, n1 = 0, n2 = 0;
	for (i = 0; i < 10; i++)
	{
		n1 = numArray[i];
		if (n1 > n2)
		{
			n2 = n1;
		}
	}
	cout << endl;
	cout << n2 << endl;
}

int main()
{
	srand(time(0));
	int numArray[10] = {};
	int size = sizeof(numArray) / sizeof(numArray[0]);

	randomArray(numArray, size);
	largestNumArray(numArray, size);

	system("pause");
	return 0;
}