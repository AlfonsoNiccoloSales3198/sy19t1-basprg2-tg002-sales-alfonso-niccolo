#include <iostream>
#include <string>
#include <conio.h>
#include <time.h>
#include "Unit.h"
#include "BuffBonus.h"

using namespace std;


int roll()
{
	int x=0;
	srand(time(NULL));

	x = rand() % 5 + 1;

	return x;
}

void buffUp()
{
	int temp = 0;
	Unit x = Unit();


	while (true)
	{
		temp = roll();

		switch (temp)
		{
		case 1:
		{
			cout << "Player will heal 10 HP" << endl;
			x.applyBuff(1);
		};  break;
		case 2:
		{
			cout << "Player's Power increased by 2" << endl;
			x.applyBuff(2);
		}; break;
		case 3:
		{
			cout << "Player's Vitality increased by 2" << endl;
			x.applyBuff(3);
		}; break;
		case 4:
		{
			cout << "Player's Dexterity increased by 2" << endl;
			x.applyBuff(4);
		}; break;
		case 5:
		{
			cout << "Player's Agility increased by 2" << endl;
			x.applyBuff(5);
		}; break;
		}


		system("pause");
		system("cls");
	}
}



int main()
{
	buffUp();
	system("pause");
	return 0;
}