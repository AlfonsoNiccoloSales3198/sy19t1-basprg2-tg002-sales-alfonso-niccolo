#pragma once
#include <iostream>
#include <string>

using namespace std;

class BuffBonus
{

	protected:

		int bonus;
		string name;
		int buff; // 1 - hp, 2 - pow, 3 - vit, 4 - dex, 5 - agi

public:

	BuffBonus()
	{
		bonus = buff = 0;
		name = "";
	}

	BuffBonus(int a, string b, int c)
	{
		bonus = a;
		name = b;
		buff = c;
	}

	int getBonus()
	{
		return bonus;
	}

	int getBuff()
	{
		return buff;
	}

	string getName()
	{
		return name;
	}

	void changeBonus(int x)
	{
		bonus = x;
	}

	void changeBuff(int x)
	{
		buff = x;
	}

	void changeName(string x)
	{
		name = x;
	}
};

class Heal : public BuffBonus
{
public:
	Heal()
	{
		name = "Heal";
		buff = 1;
		bonus = 10;
	}
};

class Might : public BuffBonus
{
public:
	Might()
	{
		name = "Might";
		buff = 2;
		bonus = 2;
	}
};

class IronSkin : public BuffBonus
{
public:
	IronSkin()
	{
		name = "Iron Skin";
		buff = 2;
		bonus = 2;
	}
};

class Concentration : public BuffBonus
{
public:
	Concentration()
	{
		name = "Concentration";
		buff = 2;
		bonus = 2;
	}
};

class Haste : public BuffBonus
{
public:
	Haste()
	{
		name = "Haste";
		buff = 2;
		bonus = 2;
	}
};