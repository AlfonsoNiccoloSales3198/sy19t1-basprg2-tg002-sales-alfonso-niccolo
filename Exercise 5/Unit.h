#include <iostream>
#include <string>
#include <vector>
#include "BuffBonus.h"

using namespace std;

class Unit
{
private:
	int maxhp;
	int curhp;
	int power;
	int vital;
	int dextr;
	int agili;
	string name;
	vector<Heal> he;
	vector<Might> m;
	vector<IronSkin> is;
	vector<Concentration> c;
	vector<Haste> ha;
public:

	Unit()
	{
		maxhp = curhp = power = vital = dextr = agili = 0;
		name = "";
	}

	Unit(int a, int b, int c, int d, int e, string f)
	{
		maxhp = curhp = a;
		power = b;
		vital = c;
		dextr = d;
		agili = e;
		name = f;
	}

	void changeMaxHp(int x)
	{
		maxhp = x;
	}

	void changeCurHp(int x)
	{
		curhp = x;
	}

	void changePower(int x)
	{
		power = x;
	}

	void changeVital(int x)
	{
		vital = x;
	}

	void changeDextr(int x)
	{
		dextr = x;
	}

	void changeAgili(int x)
	{
		agili = x;
	}

	void changeName(string x)
	{
		name = x;
	}

	int getMaxHp()
	{
		return maxhp;
	}

	int getCurHp()
	{
		return curhp;
	}

	int getPower()
	{
		return power;
	}

	int getVital()
	{
		return vital;
	}

	int getDextr()
	{
		return dextr;
	}

	int getAgili()
	{
		return agili;
	}

	string getName()
	{
		return name;
	}

	void applyBuff(int x)
	{
		int temp;
		if (x == 1)
		{
			if (getCurHp() + 10 > getMaxHp())
			{
				temp = getMaxHp() - (getCurHp() + 10);
				changeCurHp(temp);
			}
			else
			{
				changeCurHp(getCurHp() + 10);
			}
			he.push_back(Heal());
		}
		else if (x == 2)
		{
			changePower(getPower() + 2);
			m.push_back(Might());
		}
		else if (x == 3)
		{
			changeVital(getVital() + 2);
			is.push_back(IronSkin());
		}
		else if (x == 4)
		{
			changeDextr(getDextr() + 2);
			c.push_back(Concentration());
		}
		else if (x == 5)
		{
			changeAgili(getAgili() + 2);
			ha.push_back(Haste());
		}

	}

};

