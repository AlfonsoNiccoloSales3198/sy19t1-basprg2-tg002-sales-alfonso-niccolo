#define _USE_MATH_DEFINES

#include <stdio.h>
#include <string>
#include <iostream>
#include <cmath>

using namespace std;

class Shape {
public:
	int sides;
	string name;

	Shape()
	{

	}

	Shape(int x, string y)
	{
		sides = x;
		name = y;
	}
	
	void getArea()
	{

	}

	string getName()
	{
		return name;
	}

	int getSides()
	{
		return sides;
	}
};

class Circle : public Shape {
public:

	Circle()
	{

	}

	Circle(int x, string y)
	{
		sides = x;
		name = y;
	}

	void getArea()
	{
		cout << "The area is the product of Pi and the square of its radius." << endl;
	}
};

class Square : public Shape
{
public:

	Square()
	{

	}
	Square(int x, string y)
	{
		sides = x;
		name = y;
	}


	void getArea()
	{
		cout << "The area is the square of one side." << endl;
	}
};

class Rectangle : public Shape
{
public:

	Rectangle()
	{

	}

	Rectangle(int x, string y)
	{
		sides = x;
		name = y;
	}

	void getArea()
	{
		cout << "The area is the product of the length and width." << endl;
	}
};

int main()
{
	Circle c[3];
	Rectangle r[3];
	Square s[3];
	int i;

	c[0] = Circle(0, "circle");
	c[1] = Circle(0, "circle");
	c[2] = Circle(0, "circle");
	
	cout << "Circles" << endl;
	for (i = 0; i < 3; i++)
	{
		cout << c[i].getName() << " #" << (i + 1) << ": ";
		cout << c[i].getSides() << " number of sides. ";
		c[i].getArea();
	}
	r[0] = Rectangle(4, "rectangle");
	r[1] = Rectangle(4, "rectangle");
	r[2] = Rectangle(4, "rectangle");
	cout << endl << "Rectangles" << endl;
	for (i = 0; i < 3; i++)
	{
		cout << r[i].getName() << " #" << (i + 1) << ": ";
		cout << r[i].getSides() << " number of sides. ";
		r[i].getArea();
	}

	s[0] = Square(4, "square");
	s[1] = Square(4, "square");
	s[2] = Square(4, "square");
	cout << endl << "Square" << endl;
	for (i = 0; i < 3; i++)
	{
		cout << s[i].getName() << " #" << (i + 1) << ": ";
		cout << s[i].getSides() << " number of sides. ";
		s[i].getArea();
	}

}