#include "Player.h"
#include <iostream>
#include <string>
#include <conio.h>

using namespace std;

int main()
{
	Player me = Player("Cloud");
	me.replaceName("Cloud");
	cout << me.showName() << endl;
	me.replaceLvl(99);
	cout << me.showLvl() << endl;
	me.replaceCurHp(5935);
	cout << me.showCurHp() << endl;
	me.replaceBaseHp(8759);
	cout << me.showBaseHp() << endl;
	me.replaceCurMp(877);
	cout << me.showCurMp() << endl;
	me.replaceBaseMp(920);
	cout << me.showBaseMp() << endl;
	me.replaceStr(253);
	cout << me.showStr() << endl;
	me.replaceDex(255);
	cout << me.showDex() << endl;
	me.replaceVit(143);
	cout << me.showVit() << endl;
	me.replaceMagic(173);
	cout << me.showMagic() << endl;
	me.replaceSpirit(181);
	cout << me.showSpirit() << endl;
	me.replaceLuck(197);
	cout << me.showLuck() << endl;
	me.replaceAtk(255);
	cout << me.showAtk() << endl;
	me.replaceAtkPer(110);
	cout << me.showAtkPer() << endl;
	me.replaceDef(149);
	cout << me.showDef() << endl;
	me.replaceDefPer(66);
	cout << me.showDefPer() << endl;
	me.replaceMAtk(173);
	cout << me.showMAtk() << endl;
	me.replaceMDef(181);
	cout << me.showMDef() << endl;
	me.replaceMDefPer(3);
	cout << me.showMDefPer() << endl;
	me.replaceExppts(5944665);
	cout << me.showExppts() << endl;

	system("pause");
	return 0;
}