#include <string>
#pragma once

using namespace std;

class Spell
{
	public:
		string name;
		int cost;
		int damage;
		Spell()
		{
			name = "";
			cost = 0;
			damage = 0;
		}
		void renameSpell(string x)
		{
			name = x;
		}
		string callSpell()
		{
			return name;
		}
		void changeSpellCost(int x)
		{
			cost = x;
		}
		int learnSpellCost()
		{
			return cost;
		}
		void changeSpellDamage(int x)
		{
			damage = x;
		}
		int knowSpellDamage()
		{
			return damage;
		}
};
