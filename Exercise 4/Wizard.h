#include <string>
#include "Spell.h"
#pragma once

using namespace std;

class Wizard
{
	public:
		Spell s;
		int baseHp;
		int curHp;
		int baseMp;
		int curMp;
		string name;
		Wizard()
		{
			s = Spell();
			baseHp = 0;
			curHp = 0;
			baseMp = 0;
			curMp = 0;
			name = "";
		}
		void changeWizName(string x)
		{
			name = x;
		}
		string learnWizName()
		{
			return name;
		}
		void changeBaseWizHP(int x)
		{
			baseHp = x;
			curHp = x;
		}
		void changeCurWizHP(int x)
		{
			curHp = x;
		}
		int knowCurHP()
		{
			return curHp;
		}
		void changeBaseWizMP(int x)
		{
			baseMp = x;
			curMp = x;
		}
		void changeCurWizMP(int x)
		{
			curMp = x;
		}
		int knowCurMP()
		{
			return curMp;
		}
		void changeWizSpellName(string x)
		{
			s.renameSpell(x);
		}
		string callWizSpell()
		{
			return s.callSpell();
		}
		void changeWizSpellCost(int x)
		{
			s.changeSpellCost(x);
		}
		int learnWizSpellCost()
		{
			return s.learnSpellCost();
		}
		void changeWizSpellDamage(int x)
		{
			s.changeSpellDamage(x);
		}
		int learnWizSpellDamage()
		{
			return s.knowSpellDamage();
		}

};