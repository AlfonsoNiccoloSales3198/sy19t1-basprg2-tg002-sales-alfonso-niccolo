#include <iostream>
#include "Wizard.h"
#include "Spell.h"
#include <string>
#include <time.h>

using namespace std;

int main()
{
	srand(time(NULL));
	int w1Crit = 0;
	int w2Crit = 0;
	int w1Damage = 0;
	int w2Damage = 0;
	Wizard w1 = Wizard();
	Wizard w2 = Wizard();

	w1.changeWizName("Maki");
	w2.changeWizName("Aro");
	w1.changeBaseWizHP(100);
	w2.changeBaseWizHP(100);
	w1.changeBaseWizMP(100);
	w2.changeBaseWizMP(100);
	w1.changeWizSpellName("Fireball - Pusu Pusu");
	w2.changeWizSpellName("Fireball - Arrow");
	w1.changeWizSpellCost(5);
	w2.changeWizSpellCost(5);
	w1.changeWizSpellDamage(5);
	w2.changeWizSpellDamage(5);

	cout << w1.learnWizName() << " has encountered " << w2.learnWizName() << " in Asakusa. Battle start!" << endl;

	while (w1.knowCurHP() > 0 && w2.knowCurHP() > 0)
	{
		w1Crit = rand() % 5 + 1;
		w1Damage = w1.learnWizSpellDamage() * w1Crit;
		w2Crit = rand() % 5 + 1;
		w2Damage = w2.learnWizSpellDamage() * w2Crit;
		cout << w1.learnWizName() << " HP: " << w1.knowCurHP() << " MP: " << w1.knowCurMP() << endl;
		cout << w2.learnWizName() << " HP: " << w2.knowCurHP() << " MP: " << w2.knowCurMP() << endl;
		cout << w1.learnWizName() << " casts " << w1.callWizSpell() << " to " << w2.learnWizName() << endl;
		cout << w2.learnWizName() << " takes " << w1Damage << endl;
		cout << w2.learnWizName() << " casts " << w2.callWizSpell() << " to " << w1.learnWizName() << endl;
		cout << w1.learnWizName() << " takes " << w2Damage << endl;
		w1.changeCurWizHP(w1.knowCurHP() - w2Damage);
		w1.changeBaseWizMP(w1.knowCurMP() - w1.learnWizSpellCost());
		w2.changeCurWizHP(w2.knowCurHP() - w1Damage);
		w2.changeBaseWizMP(w2.knowCurMP() - w2.learnWizSpellCost());

		system("pause");
		system("cls");
	}

	if (w1.knowCurHP() == 0)
	{
		cout << w1.learnWizName() << " has fainted." << endl;
	}
	else
	{
		cout << w2.learnWizName() << " has fainted." << endl;
	}
	return 0;
}