#include<string>

using namespace std;

class Player
{
public:
	string name;
	int exppts;
	int lvl;
	int curHp;
	int baseHp;
	int curMp;
	int baseMp;
	int str;
	int dex;
	int vit;
	int magic;
	int spirit;
	int luck;
	int atk;
	int atkPer;
	int def;
	int defPer;
	int mAtk;
	int mDef;
	int mDefPer;
	Player(string x)
	{
		name = x;
	}
	void replaceName(string x)
	{
		name = x;
	}
	string showName()
	{
		return name;
	}
	void replaceExppts(int x)
	{
		exppts = x;
	}
	int showExppts()
	{
		return exppts;
	}
	void replaceLvl(int x)
	{
		lvl = x;
	}
	int showLvl()
	{
		return lvl;
	}
	void replaceCurHp(int x)
	{
		curHp = x;
	}
	int showCurHp()
	{
		return curHp;
	}
	void replaceCurMp(int x)
	{
		curMp = x;
	}
	int showCurMp()
	{
		return curMp;
	}
	void replaceBaseHp(int x)
	{
		baseHp = x;
	}
	int showBaseHp()
	{
		return baseHp;
	}
	void replaceBaseMp(int x)
	{
		baseMp = x;
	}
	int showBaseMp()
	{
		return baseMp;
	}
	void replaceStr(int x)
	{
		str = x;
	}
	int showStr()
	{
		return str;
	}
	void replaceDex(int x)
	{
		dex = x;
	}
	int showDex()
	{
		return dex;
	}
	void replaceVit(int x)
	{
		vit = x;
	}
	int showVit()
	{
		return vit;
	}
	void replaceMagic(int x)
	{
		magic = x;
	}
	int showMagic()
	{
		return magic;
	}
	void replaceSpirit(int x)
	{
		spirit = x;
	}
	int showSpirit()
	{
		return spirit;
	}
	void replaceLuck(int x)
	{
		luck = x;
	}
	int showLuck()
	{
		return luck;
	}
	void replaceAtk(int x)
	{
		atk = x;
	}
	int showAtk()
	{
		return atk;
	}
	void replaceAtkPer(int x)
	{
		atkPer = x;
	}
	int showAtkPer()
	{
		return atkPer;
	}
	void replaceDef(int x)
	{
		def = x;
	}
	int showDef()
	{
		return def;
	}
	void replaceDefPer(int x)
	{
		defPer = x;
	}
	int showDefPer()
	{
		return defPer;
	}
	void replaceMAtk(int x)
	{
		mAtk = x;
	}
	int showMAtk()
	{
		return mAtk;
	}
	void replaceMDef(int x)
	{
		mDef = x;
	}
	int showMDef()
	{
		return mDef;
	}
	void replaceMDefPer(int x)
	{
		mDefPer = x;
	}
	int showMDefPer()
	{
		return mDefPer;
	}
};
