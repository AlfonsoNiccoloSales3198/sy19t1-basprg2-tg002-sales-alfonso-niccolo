#include<iostream>
#include<string>
#include<time.h>
#include<conio.h>

using namespace std;

void arraySort(int numArray[7], int size)
{
	int temp, i, j, n=0;
	for (i = 0; i < size; i++)
	{
		for (j = i + 1; j < size; j++)
		{

			if (numArray[j] < numArray[i])
			{
				temp = numArray[i];
				numArray[i] = numArray[j];
				numArray[j] = temp;
			}
		}
	}

	for (i = 0; i < size; i++)
	{
		n++;
		cout << "Package" << n << ": " << numArray[i] << endl;
	}
}

int userBuyCompare(int userBuy, int userGold, int packageCost[7])
{
	int i, n = 0;
	for (i = 0; i < 7; i++)
	{
		if (packageCost[i] <= userGold)
		{
			n = i;
		}
	}
	if (userGold < packageCost[0])
	{
	cout << "There is no package that you can buy with your gold." << endl;
	return userGold;
	}
	else if (userBuy > userGold)
	{
		cout << "You don't have enough gold for this purchase." << endl;
		cout << "May i suggest the package " << n+1 << " valued at: " << packageCost[n] << endl;
		return userGold;
	}
	else
	{
		userGold = userGold - userBuy;
		return userGold;
	}
}

int main()
{
	int packageCost[7] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	int size = sizeof(packageCost) / sizeof(packageCost[0]);
	int userGold = 250;
	int userBuy = 0;
	char buyAgain = 'y';
	bool buyA = true;
	char input = 'y';

	while (true)
	{
		cout << "Current gold: " << userGold << endl;
		cout << endl;
		arraySort(packageCost, size);
		if (input == buyAgain)
		{
			cout << "Please enter the value of what you want to buy" << endl;
			cin >> userBuy;

			userGold = userBuyCompare(userBuy, userGold, packageCost);

			cout << "Current gold: " << userGold << endl;
			do 
			{
				cout << "Would you like to continue shopping?" << endl;
				cout << "Yes (y)/No (n)" << endl;
				input = _getch();
			} while (input != 'y' && input != 'n');
			
			system("pause");
			system("cls");
		}
		else if (input == 'n')
		{
			break;
		}
	}
	system("pause");
	return 0;
}