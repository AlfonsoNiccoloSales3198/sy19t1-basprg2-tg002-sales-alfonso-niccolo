#include <string>
#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace std;

struct turn
{
	int roundno = 0;
	string side = "";
	bool win = false;
	string card = "";
	turn* next = NULL;
};

struct card
{
	string suit = "";
	card* next = NULL;
};

void inputBet(int* input, int total)
{
	int result = -1;
	while (result <= 0 || result + total <= 0 || result + total > 30)
	{
		cout << "What is your bet? (1 to " << 30 - total << ")" << endl;
		cin >> result;
	}
	*input = result;
}

int roll(int limit)
{
	int result;
	int i;
	for (i = 0; i < 5; i++)
	{
		srand(time(NULL));
		result = rand() % limit + 1;
	}
	return result;
}

void createHand(turn* cur, string who, card** nHand)
{
	card* result = new card;
	card* curCard = new card;
	card* temp;
	int i;

	for (i = 0; i < 4; i++)
	{
		temp = new card;
		temp->suit = "citi";

		if (i == 0)
		{
			result = temp;
			curCard = temp;
		}
		else
		{
			curCard->next = temp;
			curCard = temp;
		}
	}

	temp = new card;

	switch (cur->roundno)
	{
	case 1:
	case 2:
	case 3:
	case 7:
	case 8:
	case 9:
	{
		if (who == "pla")
		{
			temp->suit = "emp";
		}
		else
		{
			temp->suit = "sla";
		}
	}; break;
	case 4:
	case 5:
	case 6:
	case 10:
	case 11:
	case 12:
	{
		if (who == "pla")
		{
			temp->suit = "sla";
		}
		else
		{
			temp->suit = "emp";
		}
	}; break;
	}

	curCard->next = temp;
	*nHand = result;
}

void deleteCard(card** nHand, int index)
{
	card* toDelete = *nHand;
	card* dPrev = NULL;
	card* dNext = NULL;
	int i = 1;

	while (i < index)
	{
		i++;
		dPrev = toDelete;
		toDelete = toDelete->next;
		dNext = toDelete->next;
	}

	if (dPrev == NULL && dNext == NULL)
	{
		*nHand = toDelete->next;
	}
	else if (dPrev != NULL && toDelete != NULL && dNext != NULL)
	{
		dPrev->next = dNext;
	}
	else if (dPrev != NULL && toDelete != NULL && dNext == NULL)
	{
		dPrev->next = NULL;
	}

}

bool playPhase(card** player, card** ai, turn** cur)
{
	int count;
	int j;
	int input;
	int aimove;
	string curTurn = (*cur)->side;

	for (count = 5; count >= 0; count--)
	{
		card* view = *player;

		cout << "Which of the following cards do you want to play?" << endl;
		for (j = 1; j <= count; j++)
		{
			cout << j << ": " << view->suit << endl;
			view = view->next;
		}

		cin >> input;
		aimove = roll(count);

		//win using emperor
		if (curTurn == "emp" &&
			input == count && aimove >= 1 && aimove < count)
		{
			cout << "You played the emperor card, which beat your opponent's civilian!" << endl;
			(*cur)->win = true;
			(*cur)->card = "emp";
			return true;
		}

		//win using slave
		else if (curTurn == "sla" &&
			input == count && aimove == count)
		{
			cout << "You played the slave card, which beat your opponent's emperor!" << endl;
			(*cur)->win = true;
			(*cur)->card = "sla";
			return true;
		}

		//win using civilian
		else if (curTurn == "emp" &&
			input >= 1 && input < count && aimove == count)
		{
			cout << "You played the civilian card, which beat your opponent's slave!" << endl;
			(*cur)->win = true;
			(*cur)->card = "civ";
			return true;
		}

		//lose using emperor
		else if (curTurn == "emp" &&
			input == count && aimove == count)
		{
			cout << "You have been defeated in this round after playiong an emperor card against your opponent's slave." << endl;
			return true;
		}

		//lose using slave
		else if (curTurn == "sla" &&
			input == count && aimove >= 1 && aimove < count)
		{
			cout << "You have been defeated in this round after playing a slave card gainst the opponent's civilian." << endl;
			return true;
		}

		//lose using civilian
		else if (curTurn == "sla" &&
			input >= 1 && input < count && aimove == count)
		{
			cout << "You have been defeated in this round after playing a civilian card against the opponent's emperor." << endl;
			return true;
		}

		//tie
		else if ((input >= 1 && input < count) &&
			(aimove >= 1 && aimove < count))
		{
			cout << "You and your opponent both played a civilian!" << endl;
		}

		deleteCard(*&player, input);
		deleteCard(*&ai, aimove);

	}
}

void showEnding(int money, int bets, int rounds, turn* finalResult)
{
	if (money >= 20000000 && bets < 30)
	{
		cout << "Best ending!" << endl;
	}
	else if (money < 20000000 && bets < 30)
	{
		cout << "Meh ending!" << endl;
	}
	else if (bets == 30 && finalResult->win != true)
	{
		cout << "Bad ending!" << endl;
	}
}

void gameLoop(turn** list)
{
	int totalmm = 0;
	int roundNo = 1;
	int bet = 0;
	int hasBetted = false;
	int roundEnd = false;
	int totalWinnings = 0;
	card* phand = new card;
	card* aihand = new card;

	while (roundNo != 12 && totalmm != 30)
	{
		cout << "Total winnings : " << totalWinnings << endl;
		cout << "Round " << roundNo << endl;
		cout << "Millimeters left before eardrum destroyed: " << 30 - totalmm << endl;

		if (!hasBetted)
		{
			inputBet(&bet, totalmm);
			cout << "You have betted " << bet << " mm." << endl;
			hasBetted = true;

			system("pause");
			system("cls");
		}

		while (!roundEnd)
		{
			createHand(*list, "pla", &phand);
			createHand(*list, "ai", &aihand);
			cout << "Total winnings : " << totalWinnings << endl;
			cout << "Round " << roundNo << endl;
			cout << "Millimeters left before eardrum destroyed: " << 30 - totalmm << endl;
			cout << "Current bet in millimeters: " << bet << endl;
			roundEnd = playPhase(&phand, &aihand, *&list);
		}

		//win
		if ((*list)->win == true && (*list)->card == "emp")
		{
			cout << "You won " << bet * 100000 << "!" << endl;
			totalWinnings = totalWinnings + (bet * 100000);
		}
		else if ((*list)->win == true && (*list)->card == "sla")
		{
			cout << "You won " << bet * 500000 << "!" << endl;
			totalWinnings = totalWinnings + (bet * 500000);
		}
		//win condition for slave?
		else if ((*list)->win == true && (*list)->card == "civ")
		{
			cout << "You did not win money. But hey, at least your ear's still safe." << endl;
		}

		//lose
		else if ((*list)->win != true)
		{
			cout << "You feel the pain accompanied by how far the contraption goes into your ear." << endl;
			totalmm = totalmm + bet;
		}

		system("pause");
		system("cls");
		*list = (*list)->next;
		roundNo = roundNo + 1;
		hasBetted = false;
		roundEnd = false;
	}

	showEnding(totalWinnings, totalmm, roundNo, *list);
}


int main()
{
	turn* turns = new turn;
	turn* cur = new turn;
	int i;

	for (i = 0; i < 12; i++)
	{
		turn* temp = new turn;
		temp->roundno = i + 1;
		switch (i)
		{
		case 0:
		case 1:
		case 2:
		case 6:
		case 7:
		case 8:
		{
			temp->side = "emp";
		}; break;
		case 3:
		case 4:
		case 5:
		case 9:
		case 10:
		case 11:
		{
			temp->side = "sla";
		}; break;
		}

		if (i == 0)
		{
			turns = temp;
			cur = temp;
		}
		else
		{
			cur->next = temp;
			cur = temp;
		}
	}

	gameLoop(&turns);

	system("pause");
	return 0;
}