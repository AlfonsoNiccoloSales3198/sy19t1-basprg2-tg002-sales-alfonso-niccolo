#include <iostream>
#include <string>
#include <vector>
#include "BuffBonus.h"

#pragma once

using namespace std;

class Unit
{
private:
	int rarityPoint;
	int health;
	int crystals;
	string name;
	vector<SSR> ssr;
	vector<SR> sr;
	vector<R> r;
	vector<HealthPotion> hp;
	vector<Bomb> b;
	vector<Crystal> c;
public:

	Unit()
	{
		rarityPoint = 0;
		crystals = health = 100;
		name = "";
	}

	Unit(int a, int b, int c, string f)
	{
		rarityPoint = a;
		health = b;
		crystals = c;
		name = f;
	}

	void changeRarPoint(int x)
	{
		rarityPoint = x;
	}

	void changeCurHp(int x)
	{
		health = x;
	}

	void changeCrystals(int x)
	{
		crystals = x;
	}

	void changeName(string x)
	{
		name = x;
	}

	int getRarPoint()
	{
		return rarityPoint;
	}

	int getCurHp()
	{
		return health;
	}

	int getCrystals()
	{
		return crystals;
	}
	
	string getName()
	{
		return name;
	}

	vector<SSR> getSSRItems()
	{
		return ssr;
	}

	vector<SR> getSRItems()
	{
		return sr;
	}

	vector<R> getRItems()
	{
		return r;
	}

	vector<HealthPotion> getHPItems()
	{
		return hp;
	}

	vector<Bomb> getBombItems()
	{
		return b;
	}

	vector<Crystal> getCrystalItems()
	{
		return c;
	}

	void applyBuff(string x)
	{
		int temp;
		if (x == "SSR")
		{
			changeRarPoint(getRarPoint() + 50);
			ssr.push_back(SSR());
		}
		else if (x == "SR")
		{
			changeRarPoint(getRarPoint() + 10);
			sr.push_back(SR());
		}
		else if (x == "R")
		{
			changeRarPoint(getRarPoint() + 1);
			r.push_back(R());
		}
		else if (x == "HealthPotion")
		{
			if ((getCurHp() + 30) > 100)
			{
				changeCurHp(100);
			}
			else
			{
				changeCurHp(getCurHp() + 30);
			}
			hp.push_back(HealthPotion());
		}
		else if (x == "Bomb")
		{
			changeCurHp(getCurHp() - 25);
			b.push_back(Bomb());
		}
		else if (x == "Crystal")
		{
			changeCrystals(getCrystals() + 15);
			c.push_back(Crystal());
		}
	}

};

