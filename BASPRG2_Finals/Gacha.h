#include<iostream>
#include<string>
#include<time.h>
#include<conio.h>
#include"Unit.h"
#include"BuffBonus.h"

#pragma once

using namespace std;

class Gacha
{
private:
	int target;
public:
	Gacha()
	{
		target = 0;
		srand(time(NULL));
	}

	void setTarget()
	{
		target = rand() % 100 + 1;
	}

	int getTarget()
	{
		return target;
	}

	BuffBonus getResult()
	{
		int temp;
		if (target == 1)
		{
			return SSR();
		}
		else if (target >= 2 && target <= 10)
		{
			return SR();
		}
		else if (target >= 11 && target <= 25)
		{
			return HealthPotion();
		}
		else if (target >= 26 && target <= 40)
		{
			return Crystal();
		}
		else if (target >= 41 && target <= 61)
		{
			return Bomb();
		}
		else if (target > 61)
		{
			return R();
		}
	}
};