#include <iostream>
#include <string>

#pragma once

using namespace std;

class BuffBonus
{

	protected:

		int bonus;
		string name;
		int buff; // 1 - SSR, 2 - SR, 3 - R, 4 - healthPotion, 5 - bomb, 6 - crystal

public:

	BuffBonus()
	{
		bonus = buff = 0;
		name = "";
	}

	BuffBonus(int a, string b, int c)
	{
		bonus = a;
		name = b;
		buff = c;
	}

	int getBonus()
	{
		return bonus;
	}

	int getBuff()
	{
		return buff;
	}

	string getName()
	{
		return name;
	}

	void changeBonus(int x)
	{
		bonus = x;
	}

	void changeBuff(int x)
	{
		buff = x;
	}

	void changeName(string x)
	{
		name = x;
	}
};

class SSR : public BuffBonus
{
public:
	SSR()
	{
		name = "SSR";
		buff = 1;
		bonus = 50;
	}
};

class SR : public BuffBonus
{
public:
	SR()
	{
		name = "SR";
		buff = 1;
		bonus = 10;
	}
};

class R : public BuffBonus
{
public:
	R()
	{
		name = "R";
		buff = 1;
		bonus = 1;
	}
};

class HealthPotion : public BuffBonus
{
public:
	HealthPotion()
	{
		name = "HealthPotion";
		buff = 2;
		bonus = 30;
	}
};

class Bomb : public BuffBonus
{
public:
	Bomb()
	{
		name = "Bomb";
		buff = 2;
		bonus = -25;
	}
};

class Crystal : public BuffBonus
{
public:
	Crystal()
	{
		name = "Crystal";
		buff = 3;
		bonus = 15;
	}
};