#include<iostream>
#include<string>
#include<time.h>
#include<conio.h>
#include"Unit.h"
#include"BuffBonus.h"
#include"Gacha.h"

using namespace std;

void displayEnd(Unit p)
{
	cout << "SSR x" << p.getSSRItems().size() << endl;
	cout << "SR x" << p.getSRItems().size() << endl;
	cout << "R x" << p.getRItems().size() << endl;
	cout << "Health Potion x" << p.getHPItems().size() << endl;
	cout << "Bomb x" << p.getBombItems().size() << endl;
	cout << "Crystals x" << p.getCrystalItems().size() << endl;
}


int main()
{
	Unit p = Unit();
	p.changeCurHp(100);
	p.changeCrystals(100);
	p.changeRarPoint(0);

	while (p.getCurHp() > 0 && p.getRarPoint() < 100 && p.getCrystals() > 0)
	{
		Gacha g = Gacha();
		g.setTarget();
		BuffBonus n = g.getResult();

		cout << "HP: " << p.getCurHp() << endl;
		cout << "Crystals: " << p.getCrystals() << endl;
		cout << "RP: " << p.getRarPoint() << endl;

		p.changeCrystals(p.getCrystals() - 5);

		cout << n.getName() << endl;
		cout << n.getBonus() << endl;

		p.applyBuff(n.getName());

		system("pause");
		system("cls");
	}
	if (p.getRarPoint() >= 100)
	{
		cout << "You win!" << endl;
	}
	else if (p.getCurHp() <= 0)
	{
		cout << "You have died..." << endl;
	}
	else if (p.getCrystals() <= 0)
	{
		cout << "You have spent all your crystals... now possibly in debt" << endl;
	}

	displayEnd(p);
	system("pause");
	return 0;
}