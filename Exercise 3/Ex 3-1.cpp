#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>

using namespace std;

void arrayPointer(int* numbers)
{
	int i;
	
	for (i = 0; i < 8; i++)
	{
		numbers[i] = rand() % 100 + 1;
	}
}

int main()
{
	int numbers[8];
	srand(time(NULL));

	arrayPointer(numbers);

	system("pause");
	return 0;
}