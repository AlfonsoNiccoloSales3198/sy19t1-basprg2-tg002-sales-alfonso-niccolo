#include<iostream>
#include<string>
#include<conio.h>
#include<time.h>

using namespace std;

int* dynamicArray(int* source)
{
	source = new int[5];

	int i;

	for (i = 0; i < 5; i++)
	{
		*(source + i) = rand() % 100 + 1;
	}

	return source;
}

void clearArray(int* source) 
{
	delete source;
	cout << "Array cleared successfully." << endl;
}

int main()
{
	srand(time(NULL));

	int* result = NULL;
	result = dynamicArray(result);

	int j;
	for (j = 0; j < 5; j++)
	{
		cout << j + 1 << ": " << *(result + j) << endl;
	}

	clearArray(result);

	system("pause");
	return 0;
}