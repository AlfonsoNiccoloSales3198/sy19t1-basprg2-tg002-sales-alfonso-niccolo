#include<iostream>
#include<string>
#include<conio.h>
#include<time.h>

using namespace std; 

struct item
{
	string name = "";
	int gold = 0;
};

int randomItem()
{
	srand(time(NULL));
	int result = rand() % 5 + 1;
	return result;
}

item *getRandomItem()
{
	int whatItem = randomItem();
	item *result = new item;

	switch (whatItem) 
	{
	case 1:
	{
		result->name = "Mithril Ore";
		result->gold = 100;
	}; break;
	case 2:
	{
		result->name = "Sharp Talon";
		result->gold = 50;
	}; break;
	case 3:
	{
		result->name = "Thick Leather";
		result->gold = 25;
	}; break;
	case 4:
	{
		result->name = "Jellopy";
		result->gold = 5;
	}; break;
	case 5:
	{
		result->name = "Cursed Stone";
		result->gold = 0;
	}; break;

	}

	return result;
}

int enterDungeon(int playerGold)
{
	item* curItem;
	int tempGold = 0;
	int curMultiplier = 1;
	char yes = 'y';
	char input;

	if (playerGold < 25)
	{
		cout << "You do not have enough gold to enter." << endl;
		return playerGold;
	}
		playerGold = playerGold - 25;
		tempGold = playerGold;
		do
		{
			curItem = getRandomItem();
			cout << "You got " << curItem->name;
			curItem->gold = curItem->gold * curMultiplier;
			cout << " with a value of " << curItem->gold << endl;
			tempGold = tempGold + curItem->gold;
			if (curItem->name == "Cursed Stone")
			{
				cout << "You die." << endl;
				return playerGold;
			}

			cout << "Do you want to continue exploring? y/n" << endl;
			input = _getch();

			if(input == yes)
			{
				cout << "Your current gold is " << tempGold << endl;
				curMultiplier++;
				system("pause");
				system("cls");
			}
			
		} while (input == yes);
		playerGold = tempGold;
		return playerGold;
}

void winCond(int playerGold)
{
	if (playerGold < 25)
	{
		cout << "You have lost the game." << endl;
	}
	else if (playerGold >= 500)
	{
		cout << "You have won the game." << endl;
	}
	else if (playerGold >= 25 && playerGold < 500)
	{
		cout << "You have exited the game with " << playerGold << "." << endl;
	}
}

int main()
{

	int playerGold = 50;
	cout << "Player Gold: " << playerGold << endl;

	playerGold = enterDungeon(playerGold);
	winCond(playerGold);

	system("pause");
	return 0;
}
